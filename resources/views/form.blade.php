<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>FORM</title>
</head>

<body>
      <!-- <form action="/sapa" method="POST">
            @csrf
            <input type="text" name="nama">
            <input type="submit" value="masok">
      </form> -->
      <h1>Buat Account Baru!</h1>
      <h3>Sign Up Form</h3>
      <form action="/welcome" method="POST">
            @csrf
            <label for="fName">First Name:</label>
            <br><br>
            <input type="text" id="fName" name="f_name">
            <br><br>

            <label for="lName">Last Name:</label>
            <br><br>
            <input type="text" id="lName" name="l_name">
            <br><br>

            <label>Gender:</label>
            <br><br>
            <input type="radio" name="gender" value="0">Male<br>
            <input type="radio" name="gender" value="1">Female<br>
            <input type="radio" name="gender" value="2">Other
            <br><br>

            <label>Nationality:</label>
            <br><br>
            <select>
                  <option value="id">Indonesian</option>
                  <option value="my">Malaysian</option>
                  <option value="sg">Singaporean</option>
                  <option value="au">Australian</option>
            </select>
            <br><br>

            <label>Language Spoken:</label>
            <br><br>
            <input type="checkbox" name="languageSpoken" value="0">Bahasa Indonesia<br>
            <input type="checkbox" name="languageSpoken" value="1">English<br>
            <input type="checkbox" name="languageSpoken" value="2">Other
            <br><br>

            <label for="bioUser">Bio:</label>
            <br><br>
            <textarea cols="30" rows="10" id="bioUser"></textarea>
            <br>

            <input type="submit" value="submit">
      </form>
</body>

</html>