<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/home', function () {
//     return view('welcome');
// });
Route::get('/home', 'HomeController@index');

// Route::get('/register/{angka}', function ($angka) {
//     return view('coba', ["angka" => $angka]);
// });
Route::get('/register', 'AuthController@form');

// Route::get('/welcome', function () {
//     return "WEEEEEEEEEELLCCCCCCCOMMMME";
// });
Route::post('/welcome', 'AuthController@wellcome');

// Route::get('user/{id}', function ($id) {
//     return 'CIdrooo coookkkkkk ' . $id;
// });

// Route::get('/form', 'RegisterController@form');

// Route::get('/sapa', 'RegisterController@sapa');
// Route::post('/sapa', 'RegisterController@sapa_post');
