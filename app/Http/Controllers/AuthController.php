<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function form()
    {
        return view('form');
    }

    public function wellcome(Request $request)
    {
        $nama_depan = $request->f_name;
        $nama_belakang = $request->l_name;
        // dd($nama_depan, $nama_belakang);

        return view('welcome-tgs', compact('nama_depan', 'nama_belakang'));
    }
}
